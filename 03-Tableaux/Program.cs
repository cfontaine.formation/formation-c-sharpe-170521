﻿using System;

namespace _03_Tableaux
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Tableau à une dimension
            // int[] tab = null;
            //  tab = new int[4];
            int[] tab = new int[4];

            // Valeur d'initialisation par défaut des éléments du tableau
            // entier ->0
            // nombre à virgule flottante -> 0.0
            // char -> '\u0000'
            // bool -> false
            // type  référence -> null

            // Accèder à un élément du tableau
            Console.WriteLine(tab[1]);
            tab[0] = 42;
            Console.WriteLine(tab[0]);

            // Length=>Nombre d'élément du tableau
            Console.WriteLine(tab.Length);

            // déclaration et initialisation
            string[] tabStr = { "azerty", "hello", "world" };
            Console.WriteLine(tabStr[2]);

            // Parcourir complétement un tableau (avec un for)
            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine($"tab[{i}]={tab[i]}");
            }

            // Parcourir complétement un tableau (foreach)
            foreach (var elm in tabStr)
            {
                Console.WriteLine(elm);
                // elm = ""; // elm est accessible uniquement en lecture
            }

            //tab[100] = 12; // si l'on essaye d'accèder à un élément en dehors du tableau => exception

            // La taille du tableau à la déclaration peut être une variable
            int l = 6;
            char[] tabChr = new char[l];
            #endregion

            #region Exercice: Tableau
            // 1 - Trouver la valeur maximale et la moyenne d’un tableau de 5 entier: -7,4,8,0,-3
            // 2 - Modifier le programme pour faire la saisie de la taille du tableau et saisir les éléments du tableau

            // int[] t = { -7, 4, 8, 0, -3 }; // 1

            int[] t; // 2
            Console.WriteLine("Entrer la taille du tableau");
            int size = Convert.ToInt32(Console.ReadLine());
            t = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.Write($"t[{i}]=");
                t[i] = Convert.ToInt32(Console.ReadLine());
            }

            int max = t[0]; // Int32.MinValue;
            double somme = 0.0;
            foreach (var v in t)
            {
                if (v > max)
                {
                    max = v;
                }
                somme += v;
            }
            Console.WriteLine($"maximum={max} moyenne={somme / t.Length}");
            #endregion

            #region  Tableau Multidimmension
            int[,] tab2D = new int[4, 3];   // Déclaration d'un tableau à 2 dimensions

            tab2D[0, 0] = 42;   // Accès à un élémént d'un tableau à 2 dimensions

            Console.WriteLine(tab2D.Length);        // Nombre d'élément du tableau    => 6
            Console.WriteLine(tab2D.Rank);          // Nombre de dimension du tableau => 2
            Console.WriteLine(tab2D.GetLength(0));  // Nombre de ligne du tableau => 3
            Console.WriteLine(tab2D.GetLength(1));  // Nombre de colonne => 2

            // Parcourir complétement un tableau à 2 dimension => for
            for (int j = 0; j < tab2D.GetLength(0); j++)
            {
                for (int i = 0; i < tab2D.GetLength(1); i++)
                {
                    Console.Write($"{tab2D[j, i]}  ");
                }
                Console.Write("\n");
            }

            // Parcourir complétement un tableau à 2 dimension => foreach
            foreach (var elm in tab2D)
            {
                Console.WriteLine(elm);
            }

            // Déclarer un tableau à 2 dimension en l'initialisant
            char[,] tab2Dchr = { { 'a', 'z' }, { 'e', 'r' }, { 't', 'y' } };
            foreach (var elm in tab2Dchr)
            {
                Console.WriteLine(elm);
            }

            // Tableau 3D
            // Déclaration d'un tableau à 3 dimensions
            int[,,] tab3D = new int[4, 2, 5];
            tab3D[2, 1, 1] = 12;                // Accès à un élément d'un tableau à 3 dimensions
            Console.WriteLine(tab3D.Rank);      // Nombre de dimension du tableau => 3
            for (int i = 0; i < tab3D.Rank; i++)
            {
                Console.WriteLine(tab3D.GetLength(i));
            }
            #endregion

            #region Tableau de tableau
            // Déclaration d'un tableau de tableau
            double[][] tabTc = new double[3][];
            tabTc[0] = new double[4];
            tabTc[1] = new double[2];
            tabTc[2] = new double[5];

            tabTc[1][0] = 1;    // accès à un élément

            Console.WriteLine(tabTc.Length);    // Nombre de Ligne => 3
            Console.WriteLine(tabTc[0].Length); // Nombre d'élément de la première ligne
            for (int i = 0; i < tabTc.Length; i++)
            {
                Console.WriteLine(tabTc[i].Length); // Nombre d'élément par ligne
            }

            //  Parcourir complétement un tableau de tableau => for
            for (int i = 0; i < tabTc.Length; i++)
            {
                for (int j = 0; j < tabTc[i].Length; j++)
                {
                    Console.Write($"{tabTc[i][j]}  ");
                }
                Console.WriteLine("");
            }

            //  Parcourir complétement un tableau de tableau => foreach
            foreach (double[] row in tabTc)
            {
                foreach (double elm in row)
                {
                    Console.WriteLine(elm);
                }
            }

            int[][] tabTc2 = new int[][] { new int[] { 10, 4, 5 }, new int[] { 10, 4, 5, 4, 5 } };
            foreach (var row in tabTc2)
            {
                foreach (var elm in row)
                {
                    Console.WriteLine(elm);
                }
            }
            #endregion
            Console.ReadKey();
        }
    }
}
