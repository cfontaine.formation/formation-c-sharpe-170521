﻿using System;

namespace _11_ExercicePolymorphisme
{
    class Program
    {
        static void Main(string[] args)
        {
            Terrain terrain = new Terrain();
            terrain.Ajouter(new Cercle(Couleur.ROUGE, 1.0));
            terrain.Ajouter(new Cercle(Couleur.ROUGE, 1.0));
            terrain.Ajouter(new Rectangle(Couleur.BLEU, 3.0, 3.0));
            terrain.Ajouter(new Rectangle(Couleur.BLEU, 3.0, 3.0));
            terrain.Ajouter(new Rectangle(Couleur.BLEU, 3.0, 3.0));
            terrain.Ajouter(new TriangleRectangle(Couleur.VERT, 1.0, 1.0));
            terrain.Ajouter(new TriangleRectangle(Couleur.VERT, 1.0, 1.0));
            terrain.Ajouter(new TriangleRectangle(Couleur.ORANGE, 1.0, 1.0));
            terrain.Ajouter(new Rectangle(Couleur.ORANGE, 1.0, 1.0));

            Console.WriteLine($"Surface rouge = {terrain.Surface(Couleur.ROUGE)}");
            Console.WriteLine($"Surface bleu = {terrain.Surface(Couleur.BLEU)}");
            Console.WriteLine($"Surface verte = {terrain.Surface(Couleur.VERT)}");
            Console.WriteLine($"Surface orange = {terrain.Surface(Couleur.ORANGE)}");
            Console.WriteLine($"Surface du terrain = {terrain.Surface()}");
            Console.ReadKey();
        }
    }
}
