﻿namespace _11_ExercicePolymorphisme
{
    class Terrain
    {
        private Forme[] formes;
        public int NbForme { get; set; }

        public Terrain() : this(10)
        {

        }

        public Terrain(int size)
        {
            formes = new Forme[size];
        }

        public void Ajouter(Forme f)
        {
            if (NbForme < formes.Length)
            {
                formes[NbForme] = f;
                NbForme++;
            }
        }

        public double Surface()
        {
            double somme = 0;
            for (int i = 0; i < NbForme; i++)
            {
                somme += formes[i].CalculSurface();
            }
            return somme;
        }

        public double Surface(Couleur c)
        {
            double somme = 0;
            for (int i = 0; i < NbForme; i++)
            {
                if (formes[i].CouleurF == c)
                {
                    somme += formes[i].CalculSurface();
                }
            }
            return somme;
        }
    }
}
