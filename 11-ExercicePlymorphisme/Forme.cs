﻿namespace _11_ExercicePolymorphisme
{
    enum Couleur { VERT, BLEU, ROUGE, ORANGE }
    abstract class Forme
    {
        public Couleur CouleurF { get; set; }

        public Forme(Couleur c)
        {
            CouleurF = c;
        }

        public abstract double CalculSurface();
    }
}
