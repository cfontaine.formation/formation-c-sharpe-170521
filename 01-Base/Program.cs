﻿using System;
using System.Text;

namespace _01_Base
{
    // Une enumération est un ensemble de constante
    // Par défaut, une énumération est de type int , on peut définir un autre type qui ne peut-être que de type intégral
    enum Direction : short { NORD = 90, EST = 0, SUD = 270, OUEST = 180 }

    [Flags]
    enum Jour
    {
        LUNDI = 1,
        MARDI = 2,
        MERCREDI = 4,
        JEUDI = 8,
        VENDREDI = 16,
        SAMEDI = 32,
        DIMANCHE = 64,
        WEEKEND = SAMEDI | DIMANCHE
    }

    // Définition d'une structure
    struct Point
    {
        public int X;
        public int Y;
    }
    class Program
    {
        static void Main(string[] args)
        {
            #region Variable et type simple
            // Déclaration d'une variable   type nomVariable;
            int i;
            //Console.WriteLine(i); // Erreur => on ne peut pas utiliser une variable locale non initialisée

            // Initialisation    nomVariable = valeur
            i = 42;
            Console.WriteLine(i);

            // Déclaration multiple de variable
            double hauteur, largeur;
            hauteur = 2.0;
            largeur = 123.0;
            Console.WriteLine(hauteur + " " + largeur); // + => concaténation

            // Déclaration et initialisation de variable    type nomVariable = valeur;
            int j = 12;
            int j1 = 1, j2 = 45;
            Console.WriteLine(j + " " + j1 + " " + j2);

            // Littéral booléen
            bool tst = false;  // true
            Console.WriteLine(tst);

            // Littéral caractère
            char chr = 'a';
            char utf8Chr = '\u0045';       // Caractère en UTF-8
            char utf8ChrHexa = '\x45';
            Console.WriteLine(chr + " " + utf8Chr + " " + utf8ChrHexa);

            // Littéral entier -> int par défaut
            long l = 12L;       // L->Littéral long
            uint ui = 1234U;    // U -> Litéral unsigned
            Console.WriteLine(l + " " + ui);

            // Littéral entier -> chagement de base
            int dec = 10;           // décimal (base 10) par défaut
            int hexa = 0xFF2;       // 0x => héxadécimal (base 16)
            int bin = 0b01010101;   // 0b => binaire 
            Console.WriteLine(dec + " " + hexa + " " + bin);

            // Littéral nombre à virgule flottante => par défaut double
            float f = 12.3F;        // F -> Littéral float
            decimal deci = 12.3M;   // M -> Littéral decimal
            Console.WriteLine(f + " " + deci);

            // Littéral nombre à virgule flottante
            double d2 = 100.4;
            double exp = 3.5e3;
            Console.WriteLine(d2 + " " + exp);

            // Séparateur _
            int sep = 1_000_000;
            double sep2 = 1_000.0;   // pas de séparateur en début, en fin , avant et après la virgule
            Console.WriteLine(sep + " " + sep2);

            // Type implicite -> var
            var v1 = 10.2;      // v1 -> double
            var v2 = "azerty";    // v2 -> string
            Console.WriteLine(v1 + " " + v2);

            // avec @ on peut utiliser les mots réservés comme nom de variable (à éviter) 
            int @var = 12;
            Console.WriteLine(@var);
            #endregion

            #region Conversion
            // Transtypage implicite: ( pas de perte de donnée)
            // type inférieur => type supérieur
            int tii = 123;
            long til = tii;
            double tid = tii;
            Console.WriteLine(tii + " " + til + " " + tid);

            // Transtypage explicite: cast = (nouveauType)
            double ted = 12.3;
            int tei = (int)ted;
            decimal tedc = (decimal)ted;
            Console.WriteLine(ted + " " + tei + " " + tedc);

            // Dépassement de capacité
            short sh1 = 300;            // 00000001 00101100    300
            sbyte sb1 = (sbyte)sh1;     //          00101100    44
            Console.WriteLine(sh1 + " " + sb1);

            // checked / unchecked
            // checked => checked permet d’activer explicitement le contrôle de dépassement de capacité 
            // pour les conversions et les opérations arithmétiques de type intégral
            //checked
            //{
            //    sb1 = (sbyte)sh1;
            //    Console.WriteLine(sh1 + " " + sb1);   // avec checked une exception est générée, s'il y a un dépassement de capacité
            //}

            unchecked // (par défaut )
            {
                sb1 = (sbyte)sh1;
                Console.WriteLine(sh1 + " " + sb1); // plus de vérification de dépassement de capacité
            }

            // Fonction de convertion
            // La classe Convert contient des méthodes statiques permettant de convertir un entier vers un double, ...
            // On peut aussi convertir une chaine de caractères en un type numérique
            int fc1 = 42;
            double cnv1 = Convert.ToDouble(fc1);    // Convertion d'un entier en double

            string str = "123";
            int cnv2 = Convert.ToInt32(str);
            Console.WriteLine(cnv1 + " " + cnv2);   // Convertion d'une chaine de caractère  en entier

            int cnv4 = 255;
            Console.WriteLine(Convert.ToString(cnv4));
            // Conversion d'un nombre sous forme de chaine dans une autre base
            Console.WriteLine(Convert.ToString(cnv4, 2));    // affichage en binaire
            Console.WriteLine(Convert.ToString(cnv4, 16));   // affichage en héxadécimal

            // Conversion d'une chaine de caractères en entier
            // Parse
            Console.WriteLine(Int32.Parse("123"));
            //  Console.WriteLine(Int32.Parse("azerty"));   //  Erreur => génère une exception

            // TryParse
            int cnv3;
            bool tstCnv = Int32.TryParse("123", out cnv3);  // Retourne true et la convertion est affecté à cnv3
            Console.WriteLine(tstCnv + "  " + cnv3);
            tstCnv = Int32.TryParse("azerty", out cnv3);    // Erreur => retourne false, 0 est affecté à cnv3
            Console.WriteLine(tstCnv + "  " + cnv3);
            #endregion

            #region Type référence
            StringBuilder s1 = new StringBuilder("hello");
            StringBuilder s2 = null;
            Console.WriteLine(s1 + " " + s2);

            s2 = s1;
            Console.WriteLine(s1 + " " + s2);

            s2 = null;
            s1 = null;  // str1 et str2 sont égales à null
                        // Il n'y a plus de référence sur l'objet, il éligible à la destruction par le garbage collector
            Console.WriteLine(s1 + " " + s2);
            #endregion

            #region Type nullable
            // Nullable : permet d'utiliser une valeur null avec un type valeur
            int? tn = null;

            Console.WriteLine(tn.HasValue);     // tn == null retourne false
            tn = 42;     //Conversion implicite (int vers nullable)
            Console.WriteLine(tn.HasValue);  // La propriété HasValue retourne true si  tn contient une valeur (!= null)

            Console.WriteLine(tn.Value);    // Pour récupérer la valeur, on peut utiliser la propriété Value
            int res = (int)tn;               // ou, on peut faire un cast
            Console.WriteLine(res);
            #endregion

            #region Constante
            const double PI = 3.14;
            //PI = 45;      // Erreur: on ne peut pas modifier la valeur d'une constante
            Console.WriteLine(2 * PI);
            #endregion

            #region Format de chaine de caractères
            int xi = 1;
            int yi = 3;
            string resFormat = string.Format("xi={0} yi={1}", xi, yi);
            Console.WriteLine(resFormat);

            Console.WriteLine("xi={0} yi={1}", xi, yi);     // on peut définir directement le format dans la mèthode WriteLine 

            Console.WriteLine($"xi={xi} yi={yi}");

            // Caractères spéciaux
            // \n       Nouvelle ligne
            // \r       Retour chariot
            // \f       Saut de page
            // \t       Tabulation horizontale
            // \v       Tabulation verticale
            // \0       Caractère nul
            // \a 	    Alerte
            // \b       Retour arrière
            // \\       Backslash
            // \'       Apostrophe
            // \"       Guillemet

            Console.WriteLine("c:\tmp\newfile.txt");
            // Chaînes textuelles => @ devant une littérale chaine de caractères
            // Conserve les retours à la ligne, n'interpréte pas les caractères spéciaux (utile pour les chemins de fichiers) 
            Console.WriteLine(@"c:\tmp\newfile.txt");
            #endregion

            #region Saisir une valeur au clavier
            string line = Console.ReadLine();
            Console.WriteLine(line);
            #endregion

            #region Exercices
            // Exercice: Salutation
            // Faire un programme qui:
            // - Affiche le message: Entrer votre nom
            // - Permet de saisir le nom
            // - Affiche Bonjour, complété du nom saisie
            Console.WriteLine("Entrer votre nom");
            string nom = Console.ReadLine();
            Console.WriteLine($"Bonjour, {nom}");

            // Exercice: Somme
            // Saisir 2 chiffres et afficher le résultat dans la console sous la forme 1 + 3 = 4
            int ea = Convert.ToInt32(Console.ReadLine());
            int eb = Convert.ToInt32(Console.ReadLine());
            int er = ea + eb;
            Console.WriteLine($"{ea}+{eb}={er}");
            #endregion

            #region Opérateur
            // Operateur arithméthique
            int op1 = 1;
            int op2 = 4;
            int opRes = op1 + op2;
            Console.WriteLine(opRes);
            opRes = 11 % 2;  // % => modulo (reste de division entière)
            Console.WriteLine(opRes);

            // Opérateur d'incrémentation
            // Pré-incrémentation
            int inc = 0;
            res = ++inc;
            Console.WriteLine($"res={res} inc={inc}"); // res=1 inc=1

            // Post-incrémentation
            inc = 0;
            res = inc++;
            Console.WriteLine($"res={res} inc={inc}"); // res=0 inc=1

            // Affectation composée
            double ac = 7.8;
            ac += 10.0; // équivaur à ac=ac+10.0:
            Console.WriteLine(ac);

            // Opérateur de comparaison
            ac = 1.0;
            bool testComp1 = ac > 3.0; // Une comparaison a pour résultat un booléen
            bool testComp2 = ac == 1.0;
            Console.WriteLine($"test1={testComp1} test2={testComp2}");

            // Opérateur logique
            inc = 1;
            // Opérateur Non
            bool inv = !(inc == 1); // false
            Console.WriteLine(inv);

            // Opérateur court-circuit && et ||
            // && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
            bool testA = ac > 3.0 && inc == 1;   // comme ac > 3.0 est faux,  inc == 1 n'est pas évalué
            Console.WriteLine(testA);

            // || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
            bool testB = ac < 100.0 || inc == 0;  // comme ac < 100.0 est vraie,  inc == 0 n'est pas évalué
            Console.WriteLine(testB);

            // Opérateur Binaires (bit à bit)
            byte b = 0b11010;
            Console.WriteLine(Convert.ToString(~b, 2));             // Opérateur ~ => complémént: 1-> 0 et 0-> 1
            Console.WriteLine(Convert.ToString(b & 0b110, 2));      //          et => 010
            Console.WriteLine(Convert.ToString(b | 0b110, 2));      //          ou => 11110
            Console.WriteLine(Convert.ToString(b ^ 0b110, 2));      // ou exclusif => 11100
            Console.WriteLine(Convert.ToString(b << 2, 2));         // Décalage à gauche de 2 => 1101000
            Console.WriteLine(Convert.ToString(b << 1, 2));         // Décalage à gauche de 1 => 110100
            Console.WriteLine(Convert.ToString(b >> 1, 2));         // Décalage à droite de 1 => 1101

            // L’opérateur de fusion null ?? retourne la valeur de l’opérande de gauche si elle n’est pas null
            // sinon, il évalue l’opérande de droite et retourne son résultat
            string str4 = "azerty";
            string resStr4 = str4 ?? "Default";
            Console.WriteLine(resStr4);       // azerty

            str4 = null;
            resStr4 = str4 ?? "Default";     // Default
            Console.WriteLine(resStr4);
            #endregion

            #region Promotion numérique
            int prni = 11;
            double prnd = 123.45;
            double prnRes = prni + prnd; // prni est promu en double => Le type le + petit est promu vers le +grand type des deux
            Console.WriteLine(prnRes);
            Console.WriteLine(prni / 2); // 5
            Console.WriteLine(prni / 2.0); // 5.5   prni est promu en double

            sbyte b1 = 1;
            sbyte b2 = 2;       // sbyte, byte, short, ushort, char sont promus en int
            int b3 = b1 + b2;   // b1 et b2 sont promus en int
            Console.WriteLine(b3);
            #endregion

            #region Enumération
            // dir est une variable qui ne pourra accepter que les valeurs de l'enum Direction
            Direction dir = Direction.NORD;

            // enum -> string
            // La méthode toString convertit une constante enummérée en une chaine de caractère
            string strDir = dir.ToString();
            Console.WriteLine(strDir);

            // enum ->  entier (cast)
            short iDir = (short)dir;
            Console.WriteLine(iDir);

            // string -> enum
            // La méthode Parse permet de convertir une chaine de caractère en une constante enummérée
            dir = (Direction)Enum.Parse(typeof(Direction), "SUD");
            Console.WriteLine(dir);
            // dir = (Direction)Enum.Parse(typeof(Direction), "SUD2"); // si la chaine n'existe pas dans l'énumation => exception
            //Console.WriteLine(dir.ToString());

            // int -> enum
            iDir = 180;
            if (Enum.IsDefined(typeof(Direction), iDir))   // Permet de tester si la valeur entière existe dans l'enumération
            {
                dir = (Direction)iDir;
                Console.WriteLine(dir);
            }
            else
            {
                Console.WriteLine("Valeur fausse");
            }

            // Énumération comme indicateurs binaires
            Jour jourSemaine = Jour.LUNDI | Jour.MERCREDI;
            if ((jourSemaine & Jour.LUNDI) != 0)    // teste la présence de LUNDI
            {
                Console.WriteLine("Lundi");
            }
            if ((jourSemaine & Jour.MARDI) != 0)    // teste la présence de MARDI
            {
                Console.WriteLine("Mardi");
            }
            jourSemaine |= Jour.SAMEDI;
            if ((jourSemaine & Jour.WEEKEND) != 0)
            {
                Console.WriteLine("WEEKEND");
            }

            switch (dir)
            {
                case Direction.NORD:
                    Console.WriteLine("Nord");
                    break;
                default:
                    Console.WriteLine("Autre direction");
                    break;
            }
            #endregion

            #region Structure
            Point pt1;
            pt1.X = 3;  // accès au champs X de la structure
            pt1.Y = 1;
            Console.WriteLine($"P1 (x={pt1.X} y= {pt1.Y})");

            Point pt2 = pt1;
            Console.WriteLine($"P2 (x={pt2.X} y= {pt2.Y})");

            if (pt1.X == pt2.X && pt1.Y == pt2.Y)
            {
                Console.WriteLine("Les points sont égaux");
            }

            pt2.X = 0;
            Console.WriteLine($"P1 (x={pt1.X} y= {pt1.Y})");
            Console.WriteLine($"P2 (x={pt2.X} y= {pt2.Y})");
            #endregion

            Console.ReadKey();
        }
    }
}
