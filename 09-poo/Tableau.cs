﻿using System;

namespace _09_poo
{
    class Tableau
    {
        private int[] _elements;

        public int Length { get; private set; }

        public Tableau(int capacite = 10)
        {
            _elements = new int[capacite];
        }

        // Indexeur
        public int this[int index]
        {
            get
            {
                if (index <= Length)
                {
                    return _elements[index];
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
            set
            {
                if (index <= Length)
                {
                    _elements[index] = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }

        // On peut avoir plusieurs indexeurs dans une classe, il faut que le type et le nombre de paramètre soit diférent (surcharge)
        //public int this[int ligne ,int colonne ]
        //{
        //    get
        //    {
        //        return 0;
        //    }
        //    set
        //    {
        //
        //    }
        //}

        public void Add(int valeur)
        {
            if (Length == _elements.Length - 1)
            {
                Resize(10);
            }
            _elements[Length] = valeur;
            Length++;
        }

        private void Resize(int capacite)
        {
            int[] tmp = new int[_elements.Length + capacite];
            for (int i = 0; i < _elements.Length; i++)
            {
                tmp[i] = _elements[i];
            }
            _elements = tmp;
        }
    }
}
