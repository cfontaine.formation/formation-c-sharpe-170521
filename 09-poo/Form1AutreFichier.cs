﻿using System;

namespace _09_poo
{
    // Classe partielle répartie sur plusieurs fichiers
    partial class Form1
    {
        public void MethodeB()
        {
            Console.WriteLine("Méthode B");
        }
    }

}
