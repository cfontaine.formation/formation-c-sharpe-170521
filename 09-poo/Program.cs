﻿using System;

namespace _09_poo
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Voiture
            // Appel d'une méthode de classe
            Voiture.TestMethodeClasse();

            // Appel d'une variable de classe
            Console.WriteLine($"nombre voiture={Voiture.NbVoiture}");

            // Instantiation de la classe Voiture
            Voiture v1 = new Voiture();
            Console.WriteLine($"nombre voiture={Voiture.NbVoiture}");

            // Accès à une propriété en ecriture (set)
            v1.Vitesse = 30;

            // Accès à une propriété en lecture (get)
            Console.WriteLine(v1.Vitesse);

            // Appel d’une méthode d’instance
            v1.Accelerer(10);
            v1.Freiner(15);
            Console.WriteLine(v1.Vitesse);
            Console.WriteLine(v1.EstArreter());
            v1.Arreter();
            Console.WriteLine(v1.EstArreter());

            Voiture v2 = new Voiture(10, "Honda", "Noir");
            Console.WriteLine($"nombre voiture={Voiture.NbVoiture}");
            Console.WriteLine(v2.Vitesse);

            // Test de l'appel du destructeur
            v2 = null;  // En affectant, null à la références v2.Il n'y a plus de référence sur l'objet voiture
                        // Il sera détruit lors de la prochaine execution du garbage collector
                        // Forcer l'appel le garbage collector
            GC.Collect();

            // Initialiseur d'objet
            Voiture v3 = new Voiture {/* Marque = "Ford",*/ Couleur = "Vert" };
            Console.WriteLine(v3.Couleur);
            Console.WriteLine($"nombre voiture={Voiture.NbVoiture}");
            Console.WriteLine(Voiture.EgaliteVitesse(v1, v3));

            // Agrégation
            Adresse adr1 = new Adresse("1, rue Esquermoise", "Lille", "59800");
            Personne per1 = new Personne("John", "Doe",adr1) ;
            Voiture v4 = new Voiture("opel", "Blanc", "fr-121-AB", 0, 100, per1);
            v4.Afficher();
            // Héritage
            VoiturePrioritaire vp1 = new VoiturePrioritaire();
            vp1.Accelerer(10);
            vp1.Arreter();
            vp1.AllumerGyro();
            Console.WriteLine(vp1.Gyro);
            vp1.EteindreGyro();
            Console.WriteLine($"nombre voiture={Voiture.NbVoiture}");
            

            VoiturePrioritaire vp2= new VoiturePrioritaire("Toyota","orange",0,false);
            Console.WriteLine($"nombre voiture={Voiture.NbVoiture}");
            vp2.Afficher();
            #endregion

            #region indexeur
            Tableau tab = new Tableau(4);
            tab.Add(3);
            tab.Add(7);
            tab.Add(1);
            tab.Add(6);
            tab.Add(3);
            Console.WriteLine(tab[0]);
            tab[1] = 1004;
            Console.WriteLine(tab[1]);
            Console.WriteLine(tab.Length);
            // tab[40] = 3; => exception
            #endregion

            #region Classe Statique
            //    Math m = new Math();  // Math est une classe static, on peut pas l'instancier
            // Elle ne contient que des membres de classe

            // Méthode d'extension
            string str1 = "Bonjour";
            string str2 = str1.Reverse();
            Console.WriteLine(str2);
            #endregion

            #region Classe Imbriquée
            Conteneur cnt = new Conteneur();
            cnt.Test();
            #endregion

            #region Exercice Compte Bancaire
            CompteBancaire cb = new CompteBancaire(100.0, per1);
            cb.Afficher();
            cb.Crediter(50.0);
            cb.Afficher();
            cb.Debiter(400.0);
            Console.WriteLine(cb.Solde);
            Console.WriteLine(cb.EstPositif());

            Personne per2 = new Personne("Jane", "Doe", adr1);
            CompteBancaire cb2 = new CompteBancaire(200.0, per2);
            cb2.Afficher();

            Personne per3 = new Personne("Alan", "Smithee", new Adresse("Grand Place", "Arras", "62000"));
            CompteEpargne ce = new CompteEpargne(0.95, per3);
            ce.Afficher();
            ce.CalculInterets();
            ce.Afficher();


            #endregion

            #region Exercice Point
            Point p1 = new Point(1, 2);
            Point p2 = new Point(2, 3);
            p1.Afficher();
            p1.Deplacer(1, 0);
            p1.Afficher();
            p2.Afficher();
            Console.WriteLine(p2.Norme());
            Console.WriteLine(Point.Distance(p1, p2));
            #endregion

            Console.ReadKey();
        }
    }
}
