﻿using System;

namespace _09_poo
{
    class VoiturePrioritaire : Voiture  // VoiturePrioritaire hérite de Voiture
    {
        public bool Gyro { get; private set; }

        public VoiturePrioritaire() : base()    // base => pour appeler le constructeur de la classe mère
        {
            Console.WriteLine("Constructeur défaut VoiturePrioritaire");
        }

        public VoiturePrioritaire(string marque, string couleur, int vitesse, bool gyro) : base(vitesse, marque, couleur)
        {
            Console.WriteLine("Constructeur 4 paramètres VoiturePrioritaire");
            Gyro = gyro;
        }

        public void AllumerGyro()
        {
            Gyro = true;
        }

        public void EteindreGyro()
        {
            Gyro = true;
        }

        // L'occultation => redéfinir une méthode d'une classe mère et à « casser » le lien vers la classe mère
        public new void Afficher()
        {
            // base => pour appeler une méthode de la classe mère
            base.Afficher();
            Console.WriteLine($"Gyro:{Gyro}");
        }
    }
}
