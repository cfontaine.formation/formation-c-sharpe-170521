﻿using System.Collections.Generic;

namespace _09_poo
{
    class FakeDaoVoiture
    {
        static List<Voiture> voitures = new List<Voiture>();

        // Exemple d'utilisation d'un constructeur static => FakeDao
        static FakeDaoVoiture()
        {
            voitures.Add(new Voiture(0, "Honda", "Orange"));
            voitures.Add(new Voiture(10, "Opel", "Rouge"));
            voitures.Add(new Voiture(0, "Ford", "Bleu"));
        }

        public void Delete(Voiture v)
        {
            voitures.Remove(v);
        }

        public void Creer(Voiture v)
        {
            voitures.Add(v);
        }

        public List<Voiture> FindAll()
        {
            return voitures;
        }

    }

}
