﻿using System;

namespace _09_poo
{
    class Personne
    {
        public string Prenom { get; set; }
        public string Nom { get; set; }
        public Adresse Adr { get; set; }

        public Personne(string prenom, string nom, Adresse adresse)
        {
            Prenom = prenom;
            Nom = nom;
            Adr = adresse;
        }

        public void Afficher()
        {
            Console.Write($"{Prenom} {Nom}  ");
            if (Adr != null)
            {
                Adr.Afficher();
            }
        }
    }
}
