﻿namespace _09_poo
{
    class Moteur
    {
        public int PuissanceCm { get; set; } = 10;

        public Moteur()
        {
        }

        public Moteur(int puissanceCm)
        {
            PuissanceCm = puissanceCm;
        }
    }

}
