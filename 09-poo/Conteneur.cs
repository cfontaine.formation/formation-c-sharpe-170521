﻿using System;

namespace _09_poo
{
    class Conteneur
    {
        private static string vClasse = "Variable de classe";

        private string vInstance = "Variable d'instance";

        public void Test()
        {
            Element elm = new Element();
            elm.TestVClasse();
            elm.TestVInstance(this);
        }

        // Classe imbriquée
        private class Element
        {
            public void TestVClasse()
            {
                Console.WriteLine(vClasse);  // On a accès aux variables de la classe de la classe conteneur
            }

            public void TestVInstance(Conteneur c)
            {
                Console.WriteLine(c.vInstance);
            }
        }
    }
}
