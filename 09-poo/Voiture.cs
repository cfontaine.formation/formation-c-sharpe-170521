﻿using System;

namespace _09_poo
{
    class Voiture   //  sealed class Voiture => pour empecher l'héritage de cette classe
    {
        // Variables d'instances => Etat

        // On n'a plus besoin de déclarer les variables d'instances _marque,_plaqueIma, _compteurKm, elles seront générées automatiquement par les propriétées automatique
        // private string _plaqueIma;
        // private int _compteurKm=100;
        // private string _marque;

        private string _couleur;        // Variable d'instance utilisée par la propriété Couleur (C# 7.0)
        private int _vitesse;           // Variable d'instance utilisée par la propriété Vitesse (propriété avec une condition)

        // Propriété auto-implémenté => la variable d'instance est générée par le compilateur
        public string Marque { get; }
        public string PlaqueIma { get; set; }
        public int CompteurKm { get; set; } = 100;  // On peut donner un valeur par défaut à une propriétée (littéral, expression ou une fonction)

        // Variable de classe
        // private static int nbVoiture;    // Remplacer par une propriétée static
        public static int NbVoiture { get; private set; }   // On peut associer un modificateur d'accès à get et à set. Il doit être plus restritif que le modificateur de la propriété

        // Agrégation
        public Personne Proprietaire { get; set; }

        // Composition
        public Moteur Moteur { get; private set; } = new Moteur();

        // Propriété
        public int Vitesse
        {
            get
            {
                return _vitesse;
            }
            set
            {
                if (value >= 0)
                {
                    _vitesse = value;
                }
            }
        }

        // Propriété c# 7.0
        public string Couleur
        {
            get => _couleur;
            set => _couleur = value;
        }

        // Accesseur (Java/C++) en C# on utilisera les propriétés
        // Getter (accès en lecture)
        //public int GetVitesse()
        //{
        //    return vitesse;
        //}

        // Setter (accès en écriture)
        //public void SetVitesse(int vitesse)
        //{
        //    if (vitesse > 0)
        //    {
        //        this.vitesse = vitesse;
        //    }
        //}

        // Constructeurs => On peut surcharger le constructeur
        // Constructeur par défaut
        public Voiture()
        {
            NbVoiture++;
            Console.WriteLine("Constructeur par défaut Voiture");
        }

        // this() => Chainnage de constructeur : appel du constructeur par défaut
        public Voiture(int vitesse, string marque, string couleur) : this()
        {
            Console.WriteLine("Constructeur 3 paramètres Voiture");
            Marque = marque;
            this._vitesse = vitesse;
            this._couleur = couleur;
        }

        // Chainnage de constructeur : appel du constructeur  Voiture(string marque, string couleur, int vitesse)
        public Voiture(string marque, string couleur, string plaqueIma, int vitesse, int compteurKm) : this(vitesse, marque, couleur)
        {
            Console.WriteLine("Constructeur 5 paramètres Voiture");
            PlaqueIma = plaqueIma;
            CompteurKm = compteurKm;
        }

        public Voiture(string marque, string couleur, string plaqueIma, int vitesse, int compteurKm, Personne proprietaire) : this(marque, couleur, plaqueIma, vitesse, compteurKm)
        {
            Proprietaire = proprietaire;
        }

        // Exemple de constructeur static: appelé avant la création de la première instance ou le référencement d’un membre statique
        static Voiture()
        {
            Console.WriteLine("Constructeur statique");
        }

        // Destructeur => libérer des ressources
        ~Voiture()
        {
            Console.WriteLine("Destructeur Voiture");
        }

        // Méthodes d'instances => comportement
        public void Accelerer(int vAcc)
        {
            if (vAcc > 0)
            {
                _vitesse += vAcc;
            }
        }

        public void Freiner(int vFrn)
        {
            if (vFrn > 0)
            {
                _vitesse -= vFrn; // vitesse=vitesse-vFRn
            }
        }
        public void Arreter()
        {
            _vitesse = 0;

        }

        public bool EstArreter()
        {
            return _vitesse == 0;
        }

        // Méthodes de classes
        public static void TestMethodeClasse()
        {
            Console.WriteLine("Méthode de classe");
            //vitesse = 0;  // Dans une méthode de classe, on n'a pas accès à une variable d'instance
            //EstArreter(); // ou une méthode d'instance
            Console.WriteLine(NbVoiture);    // On peut accéder dans une méthode de classe à une variable de classe
        }

        public static bool EgaliteVitesse(Voiture v1, Voiture v2)
        {
            return v1._vitesse == v2._vitesse;  // mais on peut accéder à une variable d'instance par l'intermédiare d'un objet passé en paramètre
        }

        public void Afficher()
        {
            Console.WriteLine($"{Marque} {Vitesse} ");
        }
    }
}
