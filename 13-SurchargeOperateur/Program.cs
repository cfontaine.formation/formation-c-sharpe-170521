﻿using System;

namespace _13_SurchargeOperateur
{
    class Program
    {
        static void Main(string[] args)
        {
            Point p1 = new Point(1, 3);
            Point p2 = -p1;
            Point p3 = new Point(2, 2);
            Console.WriteLine(p2);
            Console.WriteLine(p1 + p3);
            Console.WriteLine(p1 * 4);
            p3 += p1;        // quand on surcharge un opérateur, l'opérateur composé associé est aussi automatiquement surchargé
            Console.WriteLine(p3);
            //  Console.WriteLine(4*p1);
            Console.WriteLine(p1 == p3);
            Console.WriteLine(p1 != p3);

            // Exercice surcharge opérateur
            Fraction f1 = new Fraction(1, 2);
            Console.WriteLine(f1.Calculer());
            Fraction f2 = new Fraction(1, 2);
            Console.WriteLine(f1 + f2);
            Console.WriteLine(f1 * f2);
            Console.WriteLine(f1 * 3);
            Console.WriteLine(f1 == f2);
            Console.WriteLine(f1 == (f2 * 3));

            Console.ReadKey();
        }
    }
}
