﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _06_ClassesBases
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Chaine de caractère
            string str = "Hello";
            Console.WriteLine(str);
            string str2 = new string('a', 10);
            Console.WriteLine(str2);

            // Length -> Nombre de caractère de la chaine de caractère
            Console.WriteLine(str2.Length);

            // On peut accèder à un caractère de la chaine comme à un élément d'un tableau (index commence à 0)
            Console.WriteLine(str[1]);  // e

            // Concaténation 
            // avec l'opérateur + ou avec la méthode de classe Concat
            Console.WriteLine(str + " World");
            string str3 = string.Concat(str, " World");
            Console.WriteLine(str);

            // La méthode Join concatène les chaines, en les séparants par une chaine de séparation
            Console.WriteLine(string.Join(";", "azerty", "gduvdj", "gdaveb"));

            // Découpe la chaine en un tableau de sous-chaine suivant un ou plusieurs séparateur, par défaut le caractère espace
            string calcul = "42.7 - 34.4";
            string[] elms = calcul.Split(' ');
            foreach (string st in elms)
            {
                Console.WriteLine(st);
            }

            // Substring permet d'extraire une sous-chaine d'une chaine
            // de l'indice passé en paramètre juqu'à la fin de la chaine
            Console.WriteLine(str3.Substring(6));
            // ou pour le nombre de caractères passé en paramètre
            Console.WriteLine(str3.Substring(4, 3));

            // Insère la chaine à partir de l'indice passé en paramètre
            Console.WriteLine(str3.Insert(5, "-----------"));

            // Insère la chaine à partir de l'indice passé en paramètre
            Console.WriteLine(str3.Remove(5, 4));

            // StartsWith retourne true si la chaine commence par la chaine passé en paramètre
            Console.WriteLine(str3.StartsWith("Hel"));
            Console.WriteLine(str3.StartsWith("aaaa"));

            // IndexOf retourne la première occurence du caractère ou de la chaine passée en paramètre
            Console.WriteLine(str3.IndexOf('o'));       // 4
            Console.WriteLine(str3.IndexOf('o', 5));    // 7 idem mais à partir de l'indice passé en paramètre
            Console.WriteLine(str3.IndexOf('o', 8));    // retourne -1, si le caractère n'est pas trouvé

            // Remplace toutes les chaines (ou caratères) oldValue par newValue 
            Console.WriteLine(str3.Replace('o', 'a'));

            // Retourne True si la sous-chaine passée en paramètre est contenu dans la chaine
            Console.WriteLine(str3.Contains("lo"));
            Console.WriteLine(str3.Contains("aaa"));

            // Aligne les caractères à droite en les complétant par un caractère à gauche pour une longueur spécifiée
            Console.WriteLine(str3.PadLeft(45, '_'));

            // Aligne les caractères à gauche en les complétant par un caractère à droite pour une longueur spécifiée
            Console.WriteLine(str3.PadRight(45, '_'));

            // ToUpper convertie tous les caractères en majuscule
            Console.WriteLine(str3.ToUpper());

            // ToLower convertie tous les caractères en minuscule
            Console.WriteLine(str3.ToLower());

            // Trim supprime les caractères de blanc du début et de la fin de la chaine
            Console.WriteLine("   \n \t     azerty   gkhfgfk     \n \t \b".Trim());
            Console.WriteLine("   \n \t     azerty   gkhfgfk     \n \t \b".TrimStart());    // idem uniquement en début de chaine
            Console.WriteLine("   \n \t     azerty   gkhfgfk     \n \t \b".TrimEnd());      // idem uniquement en fin de chaine

            // Comparaison 0-> égale, 1-> se trouve après, -1 -> se trouve avant dans l'ordre alphabétique
            // Il existe 2 méthodes: une d'instance et de classe
            Console.WriteLine(str.CompareTo("Hello"));
            Console.WriteLine(str.CompareTo("World"));
            Console.WriteLine(string.Compare("World", str));

            // On peut tester l'égalité de 2 chaines de caractères avec Equals et l'opérateur ==
            Console.WriteLine("Hello".Equals(str));
            Console.WriteLine(str == "Hello");

            // On peut chainer l'appel des différentes méthodes
            Console.WriteLine(str3.ToUpper().Substring(6).PadLeft(15, '-').Concat(""));

            // Lorsqu'il y a beaucoup de manipulation (+de 3) sur une chaine ( concaténation et insertion, remplacement,supression de sous-chaine)
            // il vaut mieux utiliser un objet StringBuilder qui est plus performant qu'un objet String => pas de création d'objet intermédiare
            StringBuilder sb = new StringBuilder("test");
            sb.Append(34.5);
            sb.Append("Hello");
            string str4 = sb.ToString();    // Convertion d'un StringBuilder en une chaine de caractères 
            Console.WriteLine(str4);

            // Exercice Inversion de chaine
            Console.WriteLine(Inverser("Bonjour"));

            // Exercice Palindrome
            Console.WriteLine(Palindrome("Bonjour"));
            Console.WriteLine(Palindrome("  Radar  "));
            #endregion

            #region Date
            DateTime d = DateTime.Now;  // DateTime.Now => récupérer l'heure et la date courante
            Console.WriteLine(d);
            Console.WriteLine(d.Minute);
            Console.WriteLine(d.Month);

            DateTime noel2021 = new DateTime(2021, 12, 25);
            Console.WriteLine(noel2021);
            DateTime debut = new DateTime(2021, 5, 17, 9, 30, 0);
            Console.WriteLine(debut);
            Console.WriteLine(noel2021 - d);

            TimeSpan trenteJour = new TimeSpan(30, 0, 0, 0);    // TimeSpan => représente une durée
            Console.WriteLine(d.Add(trenteJour));
            Console.WriteLine(d.AddMonths(1));

            Console.WriteLine(d.ToLongDateString());
            Console.WriteLine(d.ToShortDateString());
            Console.WriteLine(d.ToLongTimeString());
            Console.WriteLine(d.ToShortTimeString());
            Console.WriteLine(d.ToString("dddd MM y"));
            Console.WriteLine(DateTime.Parse("2003/07/12"));
            #endregion

            #region Collection
            // Collection faiblement typé => elle peut contenir tous types d'objets
            ArrayList lst = new ArrayList();
            lst.Add("hello");
            lst.Add(2.0);
            Console.WriteLine(lst[1]);
            string str5 = (string)lst[0];

            // Collection fortement typée => type générique
            List<string> lstStr = new List<string>();
            lstStr.Add("Hello");
            lstStr.Add("Bonjour");
            lstStr.Add("World");
            // lstStr.Add(1);           // On ne peut plus qu'ajouter des chaines de caractères => sinon erreur de complilation
            lstStr.Add("Azerty");
            string resStr = lstStr[1];  // Accès à un élément de la liste
            lstStr[1] = "vdksbkf";

            foreach (var v in lstStr)    // Parcourir la collection complétement
            {
                Console.WriteLine(v);
            }
            Console.WriteLine(lstStr.Count);    // Nombre d'élément de la collection
            Console.WriteLine(lstStr.Min());    // Valeur minimum stocké dans la liste 
            lstStr.Reverse();                   // Inverser l'ordre de la liste
            foreach (var v in lstStr)
            {
                Console.WriteLine(v);
            }

            // Parcourrir un collection avec un Enumérateur
            IEnumerator<string> it = lstStr.GetEnumerator();
            it.Reset();
            while (it.MoveNext())
            {
                Console.WriteLine(it.Current);
            }

            // Iterateur
            foreach (var v in GetPairValues(64))
            {
                Console.WriteLine(v);
            }

            IEnumerator<int> it2 = GetPairValues().GetEnumerator();
            while (it2.MoveNext())
            {
                Console.WriteLine(it2.Current);
            }

            // Dictionary => association clé/valeur
            Dictionary<string, int> m = new Dictionary<string, int>();
            m.Add("123-RT", 3); // Add => ajout d'un valeur associé à une clé
                                // m.Add("123-RT", 3); // On ne peut pas ajouter, si la clé éxiste déjà => exception
            m.Add("15-RrT", 12);
            m.Add("1EE2-TGH", 25);

            Console.WriteLine(m["15-RrT"]);     // accès à un élément m[clé] => valeur
            m["15-RrT"] = 400;
            m.Remove("123-RT");

            // Parcourir un dictionnary
            foreach (var kv in m)
            {
                Console.WriteLine($"clé={kv.Key} valeur={kv.Value}");
            }

            #endregion
            Console.ReadKey();
        }

        static IEnumerable<int> GetPairValues(int limit = 16)
        {
            int i = 0;
            do
            {
                if (i % 2 == 0)
                {
                    yield return i;
                }
            }
            while (i++ < limit);

        }

        // Exercice Inversion de chaine
        // Écrire la fonction Inversee qui prend en paramètre une chaine et qui retourne la chaine avec les caractères inversés
        // bonjour →  ruojnob
        static string Inverser(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = str.Length - 1; i >= 0; i--)
            {
                sb.Append(str[i]);
            }
            return sb.ToString();
        }

        // Exercice  Palindrome
        // Écrire une méthode __Palindrome__ qui accepte en paramètre une chaine et qui retourne un booléen pour indiquer si c'est palindrome
        //    SOS →  true
        //    Bonjour →  false
        //    radar →  true
        static bool Palindrome(string str)
        {
            string tmp = str.ToLower().Trim();
            return tmp == Inverser(tmp);
        }
    }
}
