﻿using _07_FichierLib;
using System;

namespace _07_IO
{
    class Program
    {
        static void Main(string[] args)
        {
            Fichier.InfoLecteur();
            Fichier.InfoDossier();
            Fichier.InfoFile();

            Fichier.EcrireFichierText(@"c:\Formations\TestIO\CSharp\test.txt");
            Fichier.LireFichierText(@"c:\Formations\TestIO\CSharp\test.txt");

            Fichier.EcrireFichierBin(@"c:\Formations\TestIO\Csharp\test.bin");
            Fichier.LireFichierBin(@"c:\Formations\TestIO\Csharp\test.bin");

            // Exercice Parcourir
            Fichier.Parcourir(@"C:\Formations\TestIO");

            Console.ReadKey();
        }
    }
}
