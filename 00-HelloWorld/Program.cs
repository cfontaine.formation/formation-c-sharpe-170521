﻿using System;

namespace _00_HelloWorld
{
    /// <summary>
    /// Une Classe Hello World
    /// </summary>
    class Program
    {
        /// <summary>
        /// Le point d'entrée du programme
        /// </summary>
        /// <param name="args">arguments de la ligne de commande</param>
        static void Main(string[] args) // Un commentaire fin de ligne
        {
            /*
             * Commentaire 
             * sur plusieurs lignes
             */
            Console.WriteLine("Hello World!");
            Console.ReadKey();
        }
    }
}
