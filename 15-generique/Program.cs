﻿using System;

namespace _15_generique
{
    class Program
    {
        static void Main(string[] args)
        {
            Box<string> b1 = new Box<string>("azety", "qsdfg");
            Console.WriteLine(b1.Afficher());
            Box<double> b2 = new Box<double>(3.7, 5.8);
            Console.WriteLine(b2.Afficher());
            TestMethodGenerique.MethodeGen<string>("azerty");
            TestMethodGenerique.MethodeGen<int>(4);
            TestMethodGenerique.MethodeGen("azerty");
            TestMethodGenerique.MethodeGen(4.7);
            Console.ReadKey();
        }
    }
}
