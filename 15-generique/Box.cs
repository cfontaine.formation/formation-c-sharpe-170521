﻿namespace _15_generique
{
    class Box<T> //where T : class
    {
        public T A { get; set; }
        public T B { get; set; }

        public Box(T a, T b)
        {
            A = a;
            B = b;
        }

        public string Afficher()
        {
            return string.Format($"{A} + {B}");
        }

    }
}
