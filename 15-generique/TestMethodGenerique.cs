﻿using System;

namespace _15_generique
{
    class TestMethodGenerique
    {
        public static void MethodeGen<T>(T a)
        {
            Console.WriteLine(a);
        }
    }
}
