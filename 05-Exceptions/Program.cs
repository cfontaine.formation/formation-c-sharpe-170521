﻿using System;

namespace _05_Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int[] tab = new int[5];
                Console.WriteLine("Entrer un indice de l'élément du tableau");
                int index = Convert.ToInt32(Console.ReadLine());    // Peut générer une Exception FormatException, si on saisie autre chose qu'un nombre entier
                Console.WriteLine(tab[index]);                      // Peut générer une Exception IndexOutOfRangeException , si index est > 
                Console.WriteLine("Entrer un age");
                int age = Convert.ToInt32(Console.ReadLine());      // Peut générer une Exception FormatException, si on saisie autre chose qu'un nombre entier
                TraitementEmploye(age);
                //  File.OpenRead("nexistepas");         // Génère une Exception FileNotFoundException
                Console.WriteLine("La suite du programme");
            }
            catch (IndexOutOfRangeException e)    // Attrape les exceptions IndexOutOfRangeException
            {
                Console.WriteLine($"Index en dehors des limites du tableau message={e.Message}");    // Message => permet de récupérer le messsage de l'exception
                Console.WriteLine(e.StackTrace);
            }
            catch (FormatException e)    // Attrape les exceptions FormatException
            {
                    Console.WriteLine("Mauvais format");
            }
            catch (ArgumentException e) // Attrape les exceptions ArgumentException
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.WriteLine(e.InnerException.Message);
                Console.WriteLine(e.InnerException.StackTrace);
            }
            catch (Exception e)   // Attrape tous les autres exception
            {
                Console.WriteLine("Autre exception");
                Console.WriteLine(e.Message);

            }
            finally   // Le bloc finally est toujours éxécuté
            {
                // finally est utiliser pour libérer les ressources
            Console.WriteLine("Bloc finally");
            }
            Console.WriteLine("Fin du programme");

            // Exercice
            double a = SaisirNombre();
            string op = Console.ReadLine();
            double b = SaisirNombre();
            try
            {
                Caculatrice(a, op, b);
            }
            catch (ArithmeticException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();
        }

        static void TraitementEmploye(int age)
        {
            Console.WriteLine("Début du traitement Employe");
            try
            {
                TraitementAge(age);
            }
            catch (AgeException e) when (e.age < -10)   // when : le catch est éxécuté si la condition dans when est vrai
            {
                // Traitement partiel de l'exception AgeNegatifException
                Console.WriteLine($"traitement Employe: {e.Message}");
                // On relance l'exception
                throw new ArgumentException("age negatif traitement Employé", e);     // e =>référence à l'exception qui a provoquer l'exception
            }
            catch (AgeException e)
            {
                // Traitement partiel de l'exception AgeNegatifException
                Console.WriteLine($"traitement Employe: {e.Message}");
                Console.WriteLine("-------> age >-10");
                throw;      // On relance l'exception pour que l'utilisateur de la méthode la traite a son niveau
            }
            Console.WriteLine("Fin du traitement Employe");
        }

        static void TraitementAge(int age)
        {
            Console.WriteLine("Début du traitement Age");
            if (age < 0)
            {
                // throw new Exception(string.Format($"L'age ({age})est négatif"));
                throw new AgeException(age, "L'age est négatif");     //  throw => Lancer un exception
                                                                      //  Elle va remonter la pile d'appel des méthodes jusqu'à ce qu'elle soit traité par un try/catch
                                                                      //  si elle n'est pas traiter, aprés la méthode main => arrét du programme
            }
            Console.WriteLine("Fin du traitement Age");
        }

        // Exercice
        static void Caculatrice(double v1, string op, double v2)
        {
            switch (op)
            {
                case "+":
                    Console.WriteLine($"{v1} + {v2} = {v1 + v2}");
                    break;
                case "-":
                    Console.WriteLine($"{v1} - {v2} = {v1 - v2}");
                    break;
                case "*":
                    Console.WriteLine($" {v1} * {v2} = {v1 * v2}");
                    break;
                case "/":
                    if (v2 == 0.0)
                    {
                        throw new ArithmeticException("Division par 0");
                    }
                    else
                    {
                        Console.WriteLine($" {v1} / {v2} = { v1 / v2}");
                    }
                    break;
                default:
                    throw new Exception($"L'opérateur {op} est incorrecte");
                    //  break;
            }
        }

        static double SaisirNombre()
        {
            for (; ; )
            {
                try
                {
                    double val = Convert.ToDouble(Console.ReadLine());
                    return val;
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
    }
}
