﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_Exceptions
{
    class AgeException : Exception
    {

        public int age;
        public AgeException(int age)
        {
            this.age = age;
        }

        public AgeException(int age,string message) : base(message)
        {
            this.age = age;
        }

        public AgeException(int age, string message, Exception innerException) : base(message, innerException)
        {
            this.age = age;
        }
    }
}
