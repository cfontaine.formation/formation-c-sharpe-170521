﻿using System;
using System.Text;

namespace _04_Methodes
{
    class Program
    {
        static void Main(string[] args)
        {
            // Appel de methode
            double res = Somme(10, 23);
            Console.WriteLine(res);

            // Appel de methode (sans retour)
            Afficher(4);

            // Exercice maximum
            Console.WriteLine("Entrer 2 Nombres");
            double a = Convert.ToDouble(Console.ReadLine());
            double b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine(Maximum(a, b));

            // Exercice parité
            Console.WriteLine($"3:  {Even(3)}");
            Console.WriteLine($"6:  {Even(6)}");

            // Passage par valeur (par défaut en C#)
            // C'est une copie de la valeur du paramètre qui est transmise à la méthode
            int val = 3;
            TestParamValeur(val);
            Console.WriteLine(val); // 3

            StringBuilder s = new StringBuilder("valeur1");
            TestParamValeurObj(s);
            Console.WriteLine(s);
            StringBuilder s2 = TestParamValeurObj(s);

            // Passage de paramètre par référence
            // La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
            TestParamReference(ref val);
            Console.WriteLine(val); // 4

            // Passage de paramètre en sortie
            // Un paramètre out ne sert à la méthode qu'à retourner une valeur
            // L'argument transmis doit référencer une variable ou un élément de tableau
            // La variable n'a pas besoin d'être initialisée

            int o1,o2;
            TestParamOut(out o1,out o2);
            Console.WriteLine($"{o1},{o2}"); 

            // Déclarer la variable de retour dans les arguments pendant l'appel de la méthode
            TestParamOut(out int o3, out int o4);
            Console.WriteLine($"{o3},{o4}");

            // On peut ignorer un paramètre out en le nommant _
            TestParamOut(out _, out int o5);
            Console.WriteLine(o5);

            // Paramètre optionnel
            TestParamOptionnel(false, "Bonjour", 255);
            TestParamOptionnel(true, "World");
            TestParamOptionnel(true);

            // Paramètres nommés
            TestParamOptionnel(i: 255, b: false, str: "Bonjour");
            TestParamOptionnel(true, i: 56);

            // Nombre de paramètres variable
            TestParamVariable("azerty");
            TestParamVariable("azerty", "uiop");
            TestParamVariable();
            string[] tabStr = { "a", "b", "c", "de" };
            TestParamVariable(tabStr);

            // Exercice Tableau
            Menu();

            // Affichage des paramètres passés au programme (main)
            // En lignes de commandes: 05-methode.exe Azerty 12234 "aze rty"
            // Dans visual studio: propriétés du projet-> onglet Déboguer-> options de démarrage -> Arguments de la ligne de commande 
            foreach (var ar in args)
            {
                Console.WriteLine(ar);
            }

            // Surcharge de méthode
            Console.WriteLine(Multiplier(1, 2));        // Correspondance exacte des type des paramètres
            Console.WriteLine(Multiplier(2.0, 3.0));
            Console.WriteLine(Multiplier(4, 5.0));
            Console.WriteLine(Multiplier(1, 3, 5));
            Console.WriteLine(Multiplier(2.0, 3));      // convertion de 3 en double => appel de la méthode avec 2 double en paramètres
            Console.WriteLine(Multiplier(2.0F, 3.0F));  // convertion de 2.0F et de 3.0F en double => appel de la méthode avec 2 double en paramètres
            Console.WriteLine(Multiplier('1', 2));      // convertion de '1' en int => appel de la méthode avec 2 entiers en paramètre
            // Console.WriteLine(Multiplier(2.0M, 3.0M)); // erreur pas de conversion possible

            // Méthode récursive
            int f = Factorial(3);
            Console.WriteLine(f);
            
            //  SommeLocal(1, 2);
            
            Console.ReadKey();
        }

        static double Somme(double a, double b)
        {
            return a + b;   //  L'instruction return
                            // - Interrompt l'exécution de la méthode
                            // - Retourne la valeur de l'expression à droite
        }

        static void Afficher(int v)     // void => pas de valeur retourné
        {
            Console.WriteLine($"v={v}");
            // avec void => return; ou return peut être omis 
        }

        #region Exercice Maximum
        // Ecrire une méthode Maximum qui prends en paramètre 2 nombres et elle retourne le maximum
        static double Maximum(double v1, double v2)
        {
            //if (v1 > v2)
            //{
            //    return v1;
            //}
            //else
            //{
            //    return v2;
            //}
            return v1 > v2 ? v1 : v2;
        }
        #endregion

        #region Exercice Parité
        // Écrire une méthode even qui prend un entier en paramètre
        // Elle retourne vrai, si il est paire
        static bool Even(int val)
        {
            return val % 2 == 0;

            //if(val % 2 == 0)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }
        #endregion

        #region Passage de paramètre

        // Passage par valeur
        static void TestParamValeur(int v)
        {
            Console.WriteLine(v);
            v++;        // La modification de la valeur du paramètre v n'a pas d'influence en dehors de la méthode
            Console.WriteLine(v);
        }

        static StringBuilder TestParamValeurObj(StringBuilder sb)
        {
            Console.WriteLine(sb);
            // sb = new StringBuilder("azerty");
            sb.Append(" + valeur2");
            Console.WriteLine(sb);
            return sb;
        }

        // Passage par référence => ref
        static void TestParamReference(ref int v)
        {
            Console.WriteLine(v);
            v++;
            Console.WriteLine(v);
        }

        // Passage de paramètre en sortie => out
        static void TestParamOut(out int v, out int w)
        {
            // Console.WriteLine(v); 
            v = 23; // La méthode doit obligatoirement affecter une valeur aux paramètres out
            w = 34;
            Console.WriteLine(v + " " + w);
        }

        // On peut donner une valeur par défaut aux arguments passés par valeur
        static void TestParamOptionnel(bool b, string str = "Hello", int i = 10)
        {
            Console.WriteLine($"b={b} str={str} i={i}");
        }

        // Nombre d'arguments variable => params
        static void TestParamVariable(params string[] pv)
        {
            Console.WriteLine(pv.Length);
            foreach (var v in pv)
            {
                Console.WriteLine(v);
            }
        }
        #endregion

        #region Surcharge_de_méthode
        // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même nom, mais leurs signatures doient être différentes
        // La signature d'une méthode correspond aux types et nombre de paramètres
        // Le type de retour ne fait pas partie de la signature
        static int Multiplier(int a, int b)
        {
            Console.WriteLine("2 entiers");
            return a * b;
        }

        static double Multiplier(double a, double b)
        {
            Console.WriteLine("2 doubles");
            return a * b;
        }

        static double Multiplier(int a, double b)
        {
            Console.WriteLine("1 entier 1 double");
            return a * b;
        }

        static double Multiplier(int a, int b, int c)
        {
            Console.WriteLine("3 entiers");
            return a * b * c;
        }
        #endregion

        #region Méthode récursive
        static int Factorial(int n) // factoriel= 1* 2* … n
        {
            if (n <= 1)// condition de sortie
            {
                return 1;
            }
            else
            {
                return Factorial(n - 1) * n;
            }
        }
        #endregion

        // Méthode Locale (à partir de C# 7.0)
        static void TestMethodeLocal()
        {
            int SommeLocal(int a, int b) //
            {
                return a + b;
            }

            int res = SommeLocal(3, 5);
            res = SommeLocal(1, 5);
        }

        #region Exercice Tableau
        // Écrire un méthode qui affiche un tableau d’entier
        // Écrire une méthode qui permet de saisir :
        //  - La taille du tableau
        //  - Les éléments du tableau
        // Écrire une méthode qui calcule :
        // - le minimum d’un tableau d’entier
        // - le maximum
        // - la moyenne
        // Faire un menu qui permet de lancer ces méthodes
        static void AfficherTab(int[] tab)
        {
            Console.Write("[ ");
            foreach (var elm in tab)
            {
                Console.Write($"{elm} ");
            }
            Console.WriteLine("]");
        }

        static int[] SaisirTab()
        {
            Console.WriteLine("Entrer la taille du tableau");
            int size = Convert.ToInt32(Console.ReadLine());
            int[] t = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.Write($"t[{i}]=");
                t[i] = Convert.ToInt32(Console.ReadLine());
            }
            return t;
        }


        static void CalcTab(int[] t, out int min, out int max, out double moy)
        {
            max = t[0];
            min = t[0];
            double somme = 0.0;
            foreach (var v in t)
            {
                if (v > max)
                {
                    max = v;
                }
                if (v < min)
                {
                    min = v;
                }
                somme += v;
            }
            moy = somme / t.Length;
        }

        static void AffMenu()
        {
            Console.WriteLine("1 - Saisir le tableau");
            Console.WriteLine("2 - Afficher le tableau");
            Console.WriteLine("3 - Afficher le minimum, le maximum et la moyenne");
            Console.WriteLine("4 - Trier le tableau");
            Console.WriteLine("0 - Quitter");
        }

        static void Menu()
        {
            int[] tab = null;
            AffMenu();
            for (; ; )
            {
                Console.WriteLine("Choix=");
                string choix = Console.ReadLine();
                switch (choix)
                {
                    case "1":
                        tab = SaisirTab();
                        break;
                    case "2":
                        if (tab != null)
                        {
                            AfficherTab(tab);
                        }
                        else
                        {
                            Console.WriteLine("Le tableau n'est pas saisie");
                        }
                        break;
                    case "3":
                        if (tab != null)
                        {
                            CalcTab(tab, out int minimum, out int maximum, out double moyenne);
                            Console.WriteLine($"minimum={minimum}  maximum={maximum} moyenne={moyenne} ");
                        }
                        else
                        {
                            Console.WriteLine("Le tableau n'est pas saisie");
                        }
                        break;
                    case "4":
                        Array.Sort(tab);
                        break;
                    case "0":
                        Console.WriteLine("Au revoir!");
                        return;
                    default:
                        Console.WriteLine($"Le choix {choix} n'existe pas");
                        AffMenu();
                        break;
                }
            }

        }
        #endregion
    }
}
