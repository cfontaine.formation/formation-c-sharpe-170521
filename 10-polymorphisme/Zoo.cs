﻿using System.Collections.Generic;

namespace _10_polymorphisme
{
    class Zoo
    {
        private List<IPeutMarcher> residents = new List<IPeutMarcher>();

        public void Ajouter(IPeutMarcher e)
        {
            residents.Add(e);
        }

        public void Promenade()
        {
            foreach (var r in residents)
            {
                r.Marcher();
            }
        }
    }
}
