﻿using System;
using System.Collections.Generic;

namespace _10_polymorphisme
{
    class Chien : Animal, IPeutMarcher   // La classe Chien hérite de la classe Animal et implémente l'interface IPeutMarcher
    {
        public string Nom { get; set; }

        public Chien(int age, double poid, string nom) : base(age, poid)
        {
            Nom = nom;
        }

        public sealed override void EmettreSon()   // Redéfinition obligatoire de la méthode abstraite EmmettreUnSon de la classe Animal 
        {                                          // sealed sur une méthode interdit la redéfiniton de la méthode dans les sous-classes
            Console.WriteLine($"{Nom} aboie");
        }

        public void Marcher()   // Impléméntation de la méthode Marcher de l'interface IPeutMarcher
        {
            Console.WriteLine("Le chien marche");
        }

        public void Courir()    // Impléméntation de la méthode Courrir de l'interface  IPeutMarcher
        {
            Console.WriteLine("Le chien court");
        }

        public override string ToString()
        {
            return string.Format($"{base.ToString()} nom={Nom}");
        }

        public override bool Equals(object obj)
        {
            return obj is Chien chien &&
                   base.Equals(obj) &&
                   Nom == chien.Nom;
        }

        public override int GetHashCode()
        {
            int hashCode = 1240350572;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + Age.GetHashCode();
            hashCode = hashCode * -1521134295 + Poid.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Nom);
            return hashCode;
        }
    }
}
