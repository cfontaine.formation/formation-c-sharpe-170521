﻿namespace _10_polymorphisme
{
    class Labrador : Chien
    {
        public Labrador(int age, double poid, string nom) : base(age, poid, nom)
        {
        }

        // Comme la méthode est sealed dans la classe mère, on ne peut plus redéfinir la méthode
        //public override void EmettreSon()
        //{

        //}
    }
}
