﻿namespace _10_polymorphisme
{
    class Animalerie
    {
        public Animal[] Animaux { get; set; } = new Animal[5];

        public void Ecouter()
        {
            foreach (Animal a in Animaux)
            {
                if (a != null)
                {
                    a.EmettreSon();
                }
            }
        }
    }
}
