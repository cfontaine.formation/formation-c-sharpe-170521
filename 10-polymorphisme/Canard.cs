﻿using System;

namespace _10_polymorphisme
{
    class Canard : Animal, IPeutMarcher, IPeutVoler        // On peut implémenter plusieurs d'interface
    {
        public Canard(int age, double poid) : base(age, poid)
        {
        }

        public override void EmettreSon()
        {
            Console.WriteLine("Coin coin");
        }
        public void Marcher()
        {
            Console.WriteLine("Le Canard marche");
        }

        public void Courir()
        {
            Console.WriteLine("Le Canard court");
        }

        public void Atterir()
        {
            Console.WriteLine("Le Canard Atterit");
        }

        public void Decoler()
        {
            Console.WriteLine("Le Canard Décole");
        }
    }
}
