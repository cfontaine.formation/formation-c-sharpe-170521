﻿namespace _10_polymorphisme
{
    interface IPeutVoler
    {
        void Atterir();

        void Decoler();
    }
}
