﻿using System;

namespace _10_polymorphisme
{
    class Chat : Animal, IPeutMarcher
    {
        public int NbVie { set; get; } = 9;

        public Chat(int age, double poid, int nbVie) : base(age, poid)
        {
            NbVie = nbVie;
        }
        public override void EmettreSon()
        {
            Console.WriteLine("Le chat miaule");
        }
        public void Marcher()
        {
            Console.WriteLine("Le chat marche");
        }

        public void Courir()
        {
            Console.WriteLine("Le chat court");
        }
    }
}
