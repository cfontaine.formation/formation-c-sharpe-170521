﻿using System;
using System.Reflection;

namespace _10_polymorphisme
{
    class Program
    {
        static void Main(string[] args)
        {
            //Animal a1 = new Animal(4, 5000);   // Impossible la classe est abstraite
            //a1.EmettreSon();

            Chien c1 = new Chien(3, 3500, "Laika");
            c1.EmettreSon();

            Animal a2 = new Chien(13, 2000, "Idefix");    //  On peut créer une instance de Chien (classe fille) qui aura une référence, une référence Animal (classe mère)
                                                          // L'objet Chien sera vu comme un Animal, on ne pourra pas accèder aux propriétés et aux méthodes propre au chien (Nom,...)
            a2.EmettreSon();                              // Comme la méthode est virtual dans Animal et est rédéfinie dans Chien, c'est la méthode de Chien qui sera appelée

            Animal a3 = new Chat(4, 3000, 8);
            a3.EmettreSon();

            //if (a1 is Chien)           // test si a2 est de "type" Chien
            //{
            // Pour passer d'une super-classe à une sous-classe, il faut le faire explicitement avec un cast ou avec l'opérateur as
            // Chien ch2 =(Chien) a2;   
            //    Chien c2 = a1 as Chien;       // as equivalant à un cast pour un objet
            //    Console.WriteLine(c2.Nom);    // avec la référence ch2 (de type Chien), on a bien accès à toutes les propriétées de la classe Chien
            //}

            if (a2 is Chien c2) // Avec l'opérateur is, on peut tester si a2 est de type Chien et faire la conversion en même temps
            {
                //Chien c2 = (Chien)a2;
                // Chien c2 = a2 as Chien;
                Console.WriteLine(c2.Nom);
            }

            Animalerie an = new Animalerie();
            an.Animaux[0] = new Chien(3, 3500, "Laika");
            an.Animaux[1] = new Chat(4, 3000, 8);
            an.Animaux[2] = new Chat(14, 2000, 3);
            an.Animaux[3] = new Chien(13, 2000, "Idefix");
            an.Animaux[4] = new Chien(4, 5000, "Rolo");
            an.Ecouter();

            // Object
            // ToString()
            // Console.WriteLine(c1.ToString());
            Console.WriteLine(c1);
            //Equals
            Chien c3 = new Chien(4, 5000, "Rolo");
            Chien c4 = new Chien(4, 5000, "Rolo");
            Console.WriteLine(c3 == c4); // si l'opérateur == n'est pas redéfinit, on compare l'égalité des références comme se sont 2 objets différents => false
            Console.WriteLine(c3.Equals(c4));

            // Type
            // Type type = c4.GetType();
            Type type = typeof(Chien); // Reflexion
            Console.WriteLine(type.Name);   // Nom de la classe
            Console.WriteLine(type.BaseType.Name);  // Nom de la classe mère
            PropertyInfo[] infoProps = type.GetProperties(); // Obtenir Les infos sur toutes les propriétées
            foreach (var p in infoProps)
            {
                Console.WriteLine(p.Name);  // Nom des propriétées
            }

            // Interface
            IPeutMarcher ip1 = new Chien(4, 5000, "Rolo");   // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
            ip1.Marcher();

            Zoo z = new Zoo();
            z.Ajouter(new Chien(4, 5000, "Rolo"));
            z.Ajouter(new Chat(14, 2000, 3));
            z.Ajouter(new Canard(14, 2000));
            z.Promenade();
            Console.ReadKey();
        }
    }
}
