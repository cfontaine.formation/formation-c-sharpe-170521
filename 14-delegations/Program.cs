﻿using System;

namespace _14_delegations
{
    class Program
    {
        public delegate int Operation(int a, int b);

        public delegate bool Comparaison(int a, int b);

        // C# 1.0
        // ---------------------
        public static int Ajouter(int v1, int v2)
        {
            return v1 + v2;
        }

        public static int Multiplier(int v1, int v2)
        {
            return v1 * v2;
        }
        // ---------------------

        static void Main(string[] args)
        {
            // C#1.0
            Operation op1 = new Operation(Multiplier);
            Console.WriteLine(CalculerResultat(1, 2, new Operation(Ajouter)));
            Console.WriteLine(CalculerResultat(1, 2, op1));

            // C#2.0
            Operation op2 = delegate (int n1, int n2) { return n1 * n2; };
            Console.WriteLine(CalculerResultat(1, 2, delegate (int n1, int n2) { return n1 + n2; }));
            Console.WriteLine(CalculerResultat(1, 2, op2));

            // C#3.0
            Operation op3 = (p1, p2) => p1 * p2;
            Console.WriteLine(CalculerResultat(1, 2, (p1, p2) => p1 + p2));
            Console.WriteLine(CalculerResultat(1, 2, op3));

            int[] tab = { 5, 6, -4, 7, 9 };
            Program prg = new Program();
            prg.SortTab(tab, (n1, n2) => n1 < n2);
            foreach (var i in tab)
            {
                Console.WriteLine(i);
            }
            Console.ReadKey();
        }

        void SortTab(int[] tab, Comparaison cmp)
        {
            bool notDone = true;
            int end = tab.Length - 1;
            while (notDone)
            {
                notDone = false;
                for (int i = 0; i < end; i++)
                {
                    if (cmp(tab[i], tab[i + 1]))
                    {
                        int tmp = tab[i];
                        tab[i] = tab[i + 1];
                        tab[i + 1] = tmp;
                        notDone = true;
                    }
                }
                end--;
            }
        }

        public static int CalculerResultat(int v1, int v2, Operation operation)
        {
            return operation(v1, v2);
        }
    }
}
