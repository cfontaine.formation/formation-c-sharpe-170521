﻿using System;

namespace _02_Instructions
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Condition
            int cnd = Convert.ToInt32(Console.ReadLine());
            if (cnd > 100)
            {
                Console.WriteLine("cnd > 100");
            }
            else if (cnd == 100)
            {
                Console.WriteLine("cnd == 100");
            }
            else
            {
                Console.WriteLine("cnd < 100");
            }

            // Exercice: Trie de 2 Valeurs
            // Saisir 2 nombres et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 < 10.5
            Console.WriteLine("Saisir 2 nombres");
            double a = Convert.ToDouble(Console.ReadLine());
            double b = Convert.ToDouble(Console.ReadLine());
            if (a < b)
            {
                Console.WriteLine($"{a}<{b}");
            }
            else
            {
                Console.WriteLine($"{b}<{a}");
            }

            // Exercice: Intervalle
            // Saisir un nombre et dire s'il fait parti de l'intervalle - 4(exclus) et 7(inclus)
            Console.WriteLine("Saisir un nombre entier");
            int vi = Convert.ToInt32(Console.ReadLine());
            if (vi > -4 && vi <= 7)
            {
                Console.WriteLine($"{vi} fait parti de l'intervalle -4,7");
            }


            // Condition switch
            int jours = Convert.ToInt32(Console.ReadLine());
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    break;
                //case 6:
                //case 7:
                case int x when x > 5 && x < 8:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // Exercice: Calculatrice
            // Faire un programme calculatrice
            // Saisir dans la console
            // un double v1
            // une chaine de caractère opérateur qui a pour valeur valide: + - * /
            // un double v2

            // Afficher:
            // Le résultat de l’opération
            // Une message d’erreur si l’opérateur est incorrecte
            // Une message d’erreur si l’on fait une division par 0
            double v1 = Convert.ToDouble(Console.ReadLine());
            string op = Console.ReadLine();
            double v2 = Convert.ToDouble(Console.ReadLine());
            switch (op)
            {
                case "+":
                    Console.WriteLine($"{v1} + {v2} = {v1 + v2}");
                    break;

                case "-":
                    Console.WriteLine($"{v1} - {v2} = {v1 - v2}");
                    break;
                case "*":
                    Console.WriteLine($"{v1} x {v2} = {v1 * v2}");
                    break;
                case "/":
                    if (v2 == 0)
                    {
                        Console.WriteLine("Division par 0");
                    }
                    else
                    {
                        Console.WriteLine($"{v1} / {v2} = {v1 / v2}");
                    }
                    break;
                default:
                    Console.WriteLine($"l'opérateur {op} est incorrecte");
                    break;
            }

            // Opérateur ternaire
            double ta = Convert.ToDouble(Console.ReadLine());
            double tb = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine(ta < tb ? $"{ta}<{tb}" : $"{tb}<{ta}");
            #endregion

            #region Boucle
            // Boucle while
            int k = 0;
            while (k < 10)
            {
                Console.WriteLine($"k={k}");
                k++;
            }

            // Boucle for
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"i={i}");
            }

            // Instructions de branchement

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"i={i}");
                if (i == 3)
                {
                    break;  // break => termine la boucle
                }
            }

            //continue
            for (int i = 0; i < 10; i++)
            {
                if (i == 3)
                {
                    continue;   // continue => on passe à l'itération suivante
                }
                Console.WriteLine($"i={i}");
            }

            //goto
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Console.WriteLine($"i={i} j={j}");
                    if (i == 3)
                    {
                        goto EXIT_LOOP; // Utilisation de goto pour se branche sur le label EXIT_LOOP et quitter les 2 boucles imbriquées
                    }
                }
            }

        EXIT_LOOP: int jour = 1;
            switch (jour)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    goto default;// case 6;     // Utilisation de goto pour transférer le contrôle à un case ou à l’étiquette par défaut d’une instruction switch
                case 6:
                case 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // Exercice: Table de multiplication
            // Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9  
            //
            // 1 X 4 = 4
            // 2 X 4 = 8
            //  …
            // 9 x 4 = 36
            //
            // Si le nombre saisie est en dehors de l’intervalle 1 à 9 on arrête sinon on redemande une nouvelle valeur
            for (; ; ) // while(true)
            {
                Console.WriteLine("Enter une valeur entre 1 et 9");
                int mul = Convert.ToInt32(Console.ReadLine());

                if (mul < 1 || mul > 9)
                {
                    break;
                }
                for (int i = 1; i < 10; i++)
                {
                    Console.WriteLine($"{mul} x {i} = {mul * i}");
                }
            }

            // Exercice: Quadrillage 
            // un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne  

            // ex: pour 2 3  
            // [ ][ ]  
            // [ ][ ]  
            // [ ][ ]
            Console.Write("Saisir le nombre de colonne=");
            int col = Convert.ToInt32(Console.ReadLine());
            Console.Write("Saisir le nombre de ligne=");
            int row = Convert.ToInt32(Console.ReadLine());
            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++)
                {
                    Console.Write("[] ");
                }
                Console.WriteLine("");
            }
            #endregion
            Console.ReadKey();
        }
    }
}
